export const GET_ALL_IMAGES = 'GET_ALL_IMAGES';
export const ITEMS_HAS_ERRORED = 'ITEMS_HAS_ERRORED';
export const ITEMS_FETCH_DATA_SUCCESS = 'ITEMS_FETCH_DATA_SUCCESS';
export const ITEMS_IS_LOADING = 'ITEMS_IS_LOADING';

import { storage } from '../../Firebase';

export const getAllImages = () => {
    return { type: GET_ALL_IMAGES };
};

export const itemsHasErrored = (bool) => {
    return {
        type: ITEMS_HAS_ERRORED,
        hasErrored: bool
    };
}

export const itemsIsLoading = (bool) => {
    return {
        type: ITEMS_IS_LOADING,
        isLoading: bool
    };
}

export const itemsFetchDataSuccess = (items) => {
    return {
        type: ITEMS_FETCH_DATA_SUCCESS,
        items
    };
}

export const getImageData = () => {
    return (dispatch) => {
        storage.ref().child('images').listAll()
            .then(images => {
                dispatch(getImagesIrl(images.items));
            }).catch(error => {
                dispatch(itemsHasErrored(true));
            });
    };
};

export const getImagesIrl = (images) => {
    return async dispatch => {
        let imageUrlsList = [];
        for (const image of Array.from(images)) {
                let url = await getImageUrl(image);
                imageUrlsList.push({name: image.name.split('.')[0], url: url});
        };
        dispatch(itemsFetchDataSuccess(imageUrlsList));
    };
};

const getImageUrl = (imageRef) => {
   return new Promise((resolve, reject) => {
        imageRef.getDownloadURL().then(function(url) {
        resolve(url);
      }).catch(function(error) {
        console.log(error);
      });
    });
};