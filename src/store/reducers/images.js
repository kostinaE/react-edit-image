import {
    GET_ALL_IMAGES,
    ITEMS_FETCH_DATA_SUCCESS,
    ITEMS_HAS_ERRORED,
    ITEMS_IS_LOADING,

} from '../actions/images';
import { storage } from '../../Firebase';

const downloadImages = async () => {
    storage.ref().child('images').listAll()
        .then(function (data) {
            return data.items;
        }).catch(function (error) {
            // Handle any errors
        });
};

const defaultState = {
    imagesList: [],
};

const initialState = defaultState;

const imagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_IMAGES:
            let images = downloadImages();
            return {
                ...state,
                imagesList: images
            }
        case ITEMS_FETCH_DATA_SUCCESS:
            return {
                ...state,
                imagesList: action.items
            }
        default:
            return state;
    };
}

export default imagesReducer;