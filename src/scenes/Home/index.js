import React from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import ImagesList from './scenes/ImagesList';
import ImageEditor from './scenes/ImageEditor';

import { getImageData } from '../../store/actions/images';

export default function Home() {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getImageData());
  }, []);

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/home" component={ImagesList} />
        <Route exact path="/home/editor/:name" component={ImageEditor} />
      </Switch>
    </BrowserRouter>
  );
};