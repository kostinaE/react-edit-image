import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import iconBW from './icons/black_and_white.png';
import iconReset from './icons/reset.png';
import iconBlue from './icons/blue.png';

export default function ImageEditor({ match }) {
    let imageName = match.params.name;
    let imagesList = useSelector(state => state.images.imagesList);
    const [opacityValue, setOpacityValue] = React.useState(255);
    const [originImageData, setOriginImageData] = React.useState();
    const [filteredImageData, setFilteredImageData] = React.useState();
    const [hasFilter, setHasFilter] = React.useState(false);
    const canvasRef = React.createRef();

    React.useEffect(() => {
        if (imagesList.length > 0) {
            let selectedImage = imagesList.find(x => x.name == imageName);
            updateCanvas(selectedImage.url)
        }
    }, [imagesList]);

    React.useEffect(() => {
        if (originImageData) {
            changeImageOpacity(opacityValue);
        }
    }, [opacityValue]);

    const updateCanvas = (url) => {
        const context = canvasRef.current.getContext('2d');
        var imageCanvas = new Image();
        imageCanvas.setAttribute('crossOrigin', 'anonymous');
        imageCanvas.src = url;
        imageCanvas.onload = () => {
            context.drawImage(imageCanvas, 0, 0, 800, 800);
            const imageData = context.getImageData(0, 0, 800, 800);
            setOriginImageData(imageData);
        };
    }

    const makeBlackWhiteImage = () => {
        const fn = (r, g, b) => {
            var avg = 0.3 * r + 0.59 * g + 0.11 * b;
            return [avg, avg, avg, 255]
        };
        editImage(fn);
    };

    const makeBGR = () => {
        const fn = (r, g, b) => {
            return [b, g, r, 255];
        };
        editImage(fn);
    };

    const editImage = (fn) => {
        const context = canvasRef.current.getContext('2d');
        const imageData = context.getImageData(0, 0, 800, 800);
        const px = imageData.data;
        for (let i = 0; i < px.length; i += 4) {
            let redPx = px[i];
            let greenPx = px[i + 1];
            let bluePx = px[i + 2];
            let result = fn(redPx, greenPx, bluePx);
            px[i] = result[0];
            px[i + 1] = result[1];
            px[i + 2] = result[2];
            px[i + 3] = result[3];
        }
        setHasFilter(true);
        setFilteredImageData(imageData);
        context.putImageData(imageData, 0, 0);
    };

    const changeRangeValue = (event) => {
        setOpacityValue(event.target.value);
    };

    const changeImageOpacity = (value) => {
        let currentOpacity = value;
        const context = canvasRef.current.getContext('2d');
        const imageData = hasFilter ? filteredImageData : originImageData;
        const px = imageData.data;
        for (let i = 0; i < px.length; i += 4) {
            px[i + 3] = currentOpacity;
        }
        context.putImageData(imageData, 0, 0);
    };

    const resetFilter = () => {
        setHasFilter(false);
        const context = canvasRef.current.getContext('2d');
        context.putImageData(originImageData, 0, 0);
    };

    return (
        <BackgroundWrapper>
            <Wraper>
                {imagesList.length > 0 && (
                    <ImageEdit>
                        <div>
                            <canvas ref={canvasRef} width={800} height={800}> </canvas>
                        </div>
                        <Aside>
                            <EditButton disabled={hasFilter} icon={iconBW} onClick={makeBlackWhiteImage} />
                            <EditButton disabled={hasFilter} icon={iconBlue} onClick={makeBGR} />
                            <RangeContainer>
                                <VerticatRange
                                    type="range"
                                    onChange={changeRangeValue}
                                    orient="vertical"
                                    min="0"
                                    max="255"
                                    step="1"
                                    value={opacityValue}
                                />
                            </RangeContainer>
                        </Aside>
                        <EditButton icon={iconReset} onClick={resetFilter} />
                    </ImageEdit>
                )}
            </Wraper>
        </BackgroundWrapper>
    );
};

const BackgroundWrapper = styled.div`
    background-color: #22272B;
    height: 100vh;
    width: 100vw;
    display: flex;
    justify-content: center;
`;

const Wraper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-self: center;
`;

const ImageEdit = styled.div`
    display: flex;
`;

const Aside = styled.div`
    display: flex;
    flex-direction: column;
`;

const EditButton = styled.div`
    height: 40px;
    width: 40px;
    background-image: url(${p => p.icon});

    background-repeat: no-repeat;
    background-size: 40px;
    border: ${p => p.disabled ? '' : '1px solid #e4e4e4'};
    color: #22272B;
    line-height: 40px;
    text-align: center;
    border-radius: 5px;
    margin: 10px;
    cursor: ${p => p.disabled ? 'auto' : 'pointer'};
    pointer-events: ${p => p.disabled ? 'none' : ''};
`;

const RangeContainer = styled.div`
    align-self: center;
`;

const VerticatRange = styled.input`
    writing-mode: bt-lr; /* IE */
    -webkit-appearance: slider-vertical; /* WebKit */
    width: 8px;
    height: 175px;
    padding: 0 5px;
`;