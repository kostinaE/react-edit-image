import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

export default function Home({history}) {
    let imagesList = useSelector(state => state.images.imagesList);

    const openImage = (image) => {
        history.push(`home/editor/${image.name}`)
    };

    console.log(imagesList);
    return (
        <BackgroundWrapper>
            <Wraper>
                {imagesList.map((image, index) => (
                    <ImageConteiner key={index} onClick={() => openImage(image)}>
                        <img src={image.url} height="200" width="200" />
                    </ImageConteiner>
                ))}
            </Wraper>
        </BackgroundWrapper>
    );
};

const BackgroundWrapper = styled.div`
    background-color: #22272B;
    height: 100vh;
    width: 100vw;
`;

const Wraper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
`;

const ImageConteiner = styled.div`
    margin: 20px;
    border: 2px solid #e4e4e4;
    cursor: pointer;
`;