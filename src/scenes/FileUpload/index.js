import React from 'react';
import styled from 'styled-components';

import { storage } from '../../Firebase.js';

export default function FileUpload() {
    const [image, setImage] = React.useState(null);
    const [fileUploadProgress, setFileUploadProgress] = React.useState(0);
    const inputRef = React.useRef();

    const fileChange = (e) => {
        if (e.target.files[0]) {
            setImage(e.target.files[0]);
        }
    };

    const uploadFile = () => {
        const uploadTask = storage.ref(`images/${image.name}`).put(image);
        uploadTask.on('state_changed',
            (snapshot) => {
                setFileUploadProgress(Math.round(snapshot.bytesTransferred * 100 / snapshot.totalBytes));
            },
            (error) => {
                console.log(error);
            },
            () => {
                console.log('image upload to storge');
                storage.ref('images').child(image.name).getDownloadURL().then(url => {
                    console.log(url);
                    setImage(null);
                    setFileUploadProgress(0);
                })
            });
    };

    return (
        <BackgroundWrapper>
            <Wraper>
                <div style={{ display: 'flex' }}>
                    <ChooseFile>
                        <Placeholder>
                            {image ? `Выбран файл ${image.name}` : 'Выберите файл с компьютера'}
                        </Placeholder>
                        <ChooseFileBtn onClick={() => inputRef && inputRef.current && inputRef.current.click()}>
                            Выбрать файл
                            </ChooseFileBtn>
                        <input
                            ref={inputRef}
                            type="file"
                            onChange={fileChange}
                            style={{ display: 'none' }}
                        />
                    </ChooseFile>
                    {image && (
                        <Button onClick={uploadFile}>Upload</Button>
                    )}
                </div>
                {fileUploadProgress > 0 && (
                    <ProgressBar>
                       <ProgressValue style={{ width: `${fileUploadProgress}%` }} />
                    </ProgressBar>
                )}
            </Wraper>
        </BackgroundWrapper>
    );
}

const BackgroundWrapper = styled.div`
    background-color: #22272B;
    height: 100vh;
    width: 100vw;
    display: flex;
    justify-content: center;
`;

const Wraper = styled.div`
    align-self: center;
    display: flex;
    flex-direction: column;
`;

const Button = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  background-color: #30B4FF;
  border-radius: 4px;
  cursor: pointer;
  opacity: 1;
  transition: all 0.3s ease;
  height: 45px;
  padding: 0 16px;
  font-size: 15px;
  margin: 5px;

  &:hover {
    background-color: #5EC5FF;
  }
`;

const ChooseFile = styled.div`
  box-sizing: border-box;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 60px;
  padding-left: 12px;
  padding-right: 8px;
  background-color: #2C343A;
  border-radius: 4px;
  margin-bottom: 20px;
`;

const ProgressBar = styled.div`
  position: relative;
  height: 40px;
  background: #2C343A;
  border-radius: 2px;
  width: 100%;
  height: 30px;
`;

const ProgressValue = styled.div`
  width: 0%;
  height: 100%;
  background: #30B4FF;
  border-radius: 2px;
  transition: all 0.3s ease;
`;

const Placeholder = styled.div`
  color: #FFFFFF;
  font-size: 18px;
  line-height: 21px;
  opacity: 0.3;
  margin: 10px;
`;

const ChooseFileBtn = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 98px;
  height: 35px;
  color: #fff;
  font-size: 16px;
  background-color: #58636C;
  border-radius: 4px;
  cursor: pointer;
  transition: all 0.3s ease;
  padding: 5px;

  &:hover {
    background-color: #5F6B74;
  }
`;