import * as firebase from 'firebase';

var config = {
    apiKey: "AIzaSyDmYezrhPnChO1e8wfYiT4DG06swUc9tEc",
    authDomain: "react-editor-image.firebaseapp.com",
    databaseURL: "https://react-editor-image.firebaseio.com",
    projectId: "react-editor-image",
    storageBucket: "react-editor-image.appspot.com",
    messagingSenderId: "63489521244",
    appId: "1:63489521244:web:c6a9a76947b4f6d5761899",
    measurementId: "G-HQWQJYJQRN"
  };
  firebase.initializeApp(config);

  const storage = firebase.storage();
  
  export { storage, firebase };