import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';

import Home from './scenes/Home';
import FileUpload from './scenes/FileUpload';

import imagesReducer from './store/reducers/images';

const rootReducers = combineReducers({
  images: imagesReducer
});
const store = createStore(
  rootReducers,
  applyMiddleware(thunk)
);

document.body.style.margin = "0";

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/home" component={Home} />
          <Route exact path="/fileupload" component={FileUpload} />
          <Route path="/" component={Home}><Redirect to="/home" /></Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
